package it.stefanobertini.zebra.enums;

public enum MaxiCodeTag {
    postalCode("POST"), countryCode("CC"), serviceClass("SC"), ups5("UPS5"), zipper("ZIPPER"), fillc("FILLC"), msg("MSG"), lowPriorityMessageHeader("LPMS"), transportationHeader(
            "HEAD"), trackingNumber("TN"), standardCarrierAlphaCode("SCAC"), upsShipperNumber("SHIPPER"), julianPickupDay("PICKDAY"), shipmentIdNumber("SHIPID"), packageNofX(
            "NX"), packageWeigh("WEIGH"), addressValidation("VAL"), shipToStreetAddress("STADDR"), shipToCity("CITY"), shipToState("ST"), userExtraFields(
            "EXTRA"), endOfTextChar("EOT"), fieldSeparatorChar("GS"), formatTypeSeparatorChar("RS");

    private final String code;

    private MaxiCodeTag(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
