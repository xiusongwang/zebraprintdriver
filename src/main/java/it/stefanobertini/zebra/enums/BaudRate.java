package it.stefanobertini.zebra.enums;

public enum BaudRate {

    bauds_1200("1200"), bauds_4800("4800"), bauds_9600("9600"), bauds_19200("19200"), bauds_38400("38400"), bauds_57600("57600"), bauds_115200("115200");

    private final String code;

    private BaudRate(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
