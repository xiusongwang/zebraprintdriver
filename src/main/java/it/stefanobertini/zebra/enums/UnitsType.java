package it.stefanobertini.zebra.enums;

public enum UnitsType {

    inches("IN-INCHES"), centimeters("IN-CENTIMETERS"), millimeters("IN-MILLIMETERS"), dots("IN-DOTS");

    private final String code;

    private UnitsType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
