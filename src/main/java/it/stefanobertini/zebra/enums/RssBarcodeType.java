package it.stefanobertini.zebra.enums;

public enum RssBarcodeType {

    RSS_14("1"), RSS_14_Truncated("2"), RSS_14_Stacked("3"), RSS_14_Stacked_Omnidiretional("4"), RSS_Limited("5"), RSS_Expanded("6"), UPCA_Composite("7"), UPCE_Composite(
            "8"), EAN_13_Composite("9"), EAN_8_Composite("10"), UCC_128_Composite_A_B("11"), UCC_128_Composite_C("12");

    private final String code;

    private RssBarcodeType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
