package it.stefanobertini.zebra.enums;

public enum Orientation {
    horizontal, vertical;
}
