package it.stefanobertini.zebra.enums;

public enum QRCodeMaskNumber {

    none(""), mask0("0"), mask1("1"), mask2("2"), mask3("3"), mask4("4"), mask5("5"), mask6("6"), mask7("7");

    private final String code;

    private QRCodeMaskNumber(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
