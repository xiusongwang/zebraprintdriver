package it.stefanobertini.zebra.enums;

public enum PatternType {

    filled("100"), horizontalLines("101"), verticalLines("102"), rightRisingDiagonalLines("103"), leftRisingDiagonalLines("104"), squarePattern("105"), crossHatckPattern(
            "106");

    private final String code;

    private PatternType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
