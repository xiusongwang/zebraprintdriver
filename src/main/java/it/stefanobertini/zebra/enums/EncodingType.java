package it.stefanobertini.zebra.enums;

public enum EncodingType {

    ascii("ASCII"), utf8("UTF-8"), gb18030("GB18030");

    private final String code;

    private EncodingType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
