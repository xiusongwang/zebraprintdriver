package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.CommandTextProducerInterface;

public class ScalableConcatScalableText implements CommandTextProducerInterface {
    private String name;
    private int fontWidth;
    private int fontHeight;
    private double offset;
    private String text;

    public ScalableConcatScalableText(String name, int fontWidth, int fontHeight, double offset, String text) {
        super();
        this.name = name;
        this.fontWidth = fontWidth;
        this.fontHeight = fontHeight;
        this.offset = offset;
        this.text = text;
    }

    public String getCommandLine() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("ST ");
        buffer.append(getName());
        buffer.append(" ");
        buffer.append(getFontWidth());
        buffer.append(" ");
        buffer.append(getFontHeight());
        buffer.append(" ");
        buffer.append(getOffset());
        buffer.append(" ");
        buffer.append(getText());
        buffer.append("\r\n");
        return buffer.toString();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the fontWidth
     */
    public int getFontWidth() {
        return fontWidth;
    }

    /**
     * @return the fontHeight
     */
    public int getFontHeight() {
        return fontHeight;
    }

    /**
     * @return the offset
     */
    public double getOffset() {
        return offset;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScalableConcatText [name=" + name + ", fontWidth=" + fontWidth + ", fontHeight=" + fontHeight + ", offset=" + offset + ", text=" + text + "]";
    }

}