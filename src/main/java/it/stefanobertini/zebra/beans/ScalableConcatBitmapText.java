package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.CommandTextProducerInterface;

public class ScalableConcatBitmapText implements CommandTextProducerInterface {
    private Font font;
    private double offset;
    private String text;

    public ScalableConcatBitmapText(Font font, double offset, String text) {
        super();
        this.font = font;
        this.offset = offset;
        this.text = text;
    }

    /**
     * @return the font
     */
    public Font getFont() {
        return font;
    }

    /**
     * @param font
     *            the font to set
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * @return the offset
     */
    public double getOffset() {
        return offset;
    }

    /**
     * @param offset
     *            the offset to set
     */
    public void setOffset(double offset) {
        this.offset = offset;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScalableConcatBitmapText [font=" + font + ", offset=" + offset + ", text=" + text + "]";
    }

    public String getCommandLine() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("ST ");
        buffer.append(getFont().getCommandLine());
        buffer.append(" ");
        buffer.append(getOffset());
        buffer.append(" ");
        buffer.append(getText());
        buffer.append("\r\n");
        return buffer.toString();
    }

}