package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;

public class Move extends AbstractCommand implements LineModeCommandInterface {

    private boolean relative;
    private Position position;
    private Orientation orientation;

    public Move(double distance, Orientation orientation) {
        this(distance, orientation, false);
    }

    public Move(double distance, Orientation orientation, boolean relative) {
        Position position = new Position();

        if (Orientation.horizontal.equals(orientation)) {
            position.setX(distance);
        } else {
            position.setY(distance);
        }
        this.relative = relative;
        this.position = position;
        this.orientation = orientation;
    }

    public Move(Position position) {
        this(position, false);
    }

    public Move(Position position, boolean relative) {
        super();
        this.relative = relative;
        this.position = position;
        orientation = null;
    }

    public String getCommand() {
        StringBuffer command = new StringBuffer();
        command.append("! U1 ");
        if (relative) {
            command.append("R");
        }
        if (orientation == null || Orientation.horizontal.equals(orientation)) {
            command.append("X");
        }
        if (orientation == null || Orientation.vertical.equals(orientation)) {
            command.append("Y");
        }
        return command.toString();
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        if (orientation == null || Orientation.horizontal.equals(orientation)) {
            appendText(" ");
            appendText(position.getX());
        }
        if (orientation == null || Orientation.vertical.equals(orientation)) {
            appendText(" ");
            appendText(position.getY());
        }
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("position", position);

        // if (position.getX() == 0 && position.getY() == 0) {
        // Validator.signalError("Position cannot have both x and y set to 0");
        // }

        Validator.isMoreThanOrEqualTo("position.y", position.getY(), 0);
    }

    public boolean isRelative() {
        return relative;
    }

    public void setRelative(boolean relative) {
        this.relative = relative;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Move [relative=" + relative + ", position=" + position + "]";
    }

}
