package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetSpacing extends AbstractDoubleParameterCommand implements LineModeCommandInterface {

    public SetSpacing() {
        setParameter(0);
    }

    public SetSpacing(double spacing) {
        setParameter(spacing);
    }

    public String getCommand() {
        return "! U1 SETSP";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("spacing", getParameter(), 0);
    }

    public void setSpacing(double spacing) {
        setParameter(spacing);
    }

    public double getSpacing() {
        return getParameter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetSpacing [spacing=" + getParameter() + "]";
    }

}
