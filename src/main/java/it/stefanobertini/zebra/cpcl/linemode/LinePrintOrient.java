package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.LpOrientation;

public class LinePrintOrient extends AbstractStringParameterCommand implements LineModeCommandInterface {

    public LinePrintOrient() {
        this(LpOrientation.horizontal);
    }

    public LinePrintOrient(String orientation) {
        setParameter(orientation);
    }

    public LinePrintOrient(LpOrientation orientation) {
        setParameter(orientation.getCode());
    }

    public String getCommand() {
        return "! U1 LP-ORIENT";
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", getParameter());
    }

    /**
     * @return the orientation
     */
    public String getOrientation() {
        return getParameter();
    }

    /**
     * @param orientation
     *            the orientation to set
     */
    public void setOrientation(String orientation) {
        setParameter(orientation);
    }

    /**
     * @param orientation
     *            the orientation to set
     */
    public void setOrientation(LpOrientation orientation) {
        setParameter(orientation.getCode());
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LinePrintOrient [orientation()=" + getParameter() + "]";
    }

}
