package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

public class Beep extends AbstractCommand implements CommandInterface {

    private int length;

    public Beep() {
        this(0);
    }

    public Beep(int length) {
        super();
        this.length = length;
    }

    @Override
    public String getCommand() {
        return "BEEP";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(length);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("length", length, 0);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Beep [length=" + length + "]";
    }

}
