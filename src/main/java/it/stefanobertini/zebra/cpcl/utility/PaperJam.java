package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.PaperJamMethod;

public class PaperJam extends AbstractCommand implements CommandInterface {

    private PaperJamMethod paperJamMethod;
    private double barDistance;
    private String alertMessage;

    public PaperJam() {
        this(PaperJamMethod.presentation, 0, "");
    }

    public PaperJam(PaperJamMethod paperJamMethod, double barDistance) {
        this(paperJamMethod, barDistance, "");
    }

    public PaperJam(PaperJamMethod paperJamMethod, double barDistance, String alertMessage) {
        super();
        this.paperJamMethod = paperJamMethod;
        this.barDistance = barDistance;
        this.alertMessage = alertMessage;
    }

    @Override
    public String getCommand() {
        return "PAPER-JAM";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(paperJamMethod.getCode());
        appendText(" ");
        appendText(barDistance);
        if (alertMessage != null && alertMessage.length() > 0) {
            appendText(" ALERT \"");
            appendText(alertMessage);
            appendText("\"");
        }
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("paperJamMethod", paperJamMethod);
        Validator.isMoreThanOrEqualTo("barDistance", barDistance, 0);
    }

    public PaperJamMethod getPaperJamMethod() {
        return paperJamMethod;
    }

    public void setPaperJamMethod(PaperJamMethod paperJamMethod) {
        this.paperJamMethod = paperJamMethod;
    }

    public double getBarDistance() {
        return barDistance;
    }

    public void setBarDistance(double barDistance) {
        this.barDistance = barDistance;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    @Override
    public String toString() {
        return "PaperJam [paperJamMethod=" + paperJamMethod + ", barDistance=" + barDistance + ", alertMessage=" + alertMessage + "]";
    }

}
