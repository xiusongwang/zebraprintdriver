package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.DefineFileTerminator;

import java.util.ArrayList;
import java.util.List;

public class DefineFile extends AbstractCommand implements CommandInterface {

    private String fileName;
    private List<String> data;
    private DefineFileTerminator fileTerminator;

    public DefineFile() {
        this("", new ArrayList<String>(), DefineFileTerminator.end);
    }

    public DefineFile(String fileName) {
        this(fileName, new ArrayList<String>(), DefineFileTerminator.end);
    }

    public DefineFile(String fileName, List<String> data) {
        this(fileName, data, DefineFileTerminator.end);
    }

    public DefineFile(String fileName, List<String> data, DefineFileTerminator fileTerminator) {
        this.fileName = fileName;
        this.data = data;
        this.fileTerminator = fileTerminator;
    }

    @Override
    public String getCommand() {
        return "DF";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! ");
        appendText(getCommand());
        appendText(" ");
        appendText(fileName);
        endLine();

        for (int i = 0; i < data.size(); i++) {
            String line = data.get(i);
            appendText(line);
            endLine();
        }
        appendText(fileTerminator.getCode());
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("fileName", fileName);
        Validator.isNotEmpty("data", data);
        Validator.isRequired("fileTerminator", fileTerminator);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public DefineFileTerminator getFileTerminator() {
        return fileTerminator;
    }

    public void setFileTerminator(DefineFileTerminator fileTerminator) {
        this.fileTerminator = fileTerminator;
    }

    @Override
    public String toString() {
        return "DefineFile [fileName=" + fileName + ", fileTerminator=" + fileTerminator + ", data=" + data + "]";
    }
}
