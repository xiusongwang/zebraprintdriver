package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.CountryCode;

public class Country extends AbstractCommand implements CommandInterface {

    private String countryCode;

    public Country() {
        this(CountryCode.usa);
    }

    public Country(CountryCode countryCode) {
        this(countryCode.getCode());
    }

    public Country(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String getCommand() {
        return "COUNTRY";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(countryCode);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("countryCode", countryCode);
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode.getCode();
    }

    @Override
    public String toString() {
        return "Country [countryCode=" + countryCode + "]";
    }

}
