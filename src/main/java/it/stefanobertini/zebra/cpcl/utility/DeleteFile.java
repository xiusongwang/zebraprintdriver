package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

public class DeleteFile extends AbstractCommand implements CommandInterface {

    private String fileName;

    public DeleteFile() {
        this("");
    }

    public DeleteFile(String fileName) {
        super();
        this.fileName = fileName;
    }

    @Override
    public String getCommand() {
        return "DEL";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(fileName);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("fileName", fileName);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "Del [fileName=" + fileName + "]";
    }
}
