package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

/**
 * 
 * Implements the ! command<br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * {@code
 * ! {offset} {hRes} {vRes} {height} {qty}<br/>
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */
public class StartPrint extends AbstractCommand implements LabelModeCommandInterface {

    private double offset = 0;
    private int hRes = 200;
    private int vRes = 200;
    private double heigth = 240;
    private int quantity = 1;

    public String getCommand() {
        return "!";
    }

    public StartPrint() {
        this(0, 200, 200, 500, 1);
    }

    public StartPrint(double offset, int heigth) {
        this(offset, 200, 200, heigth, 1);
    }

    public StartPrint(double offset, double heigth, int quantity) {
        this(offset, 200, 200, heigth, quantity);
    }

    public StartPrint(double offset, int hRes, int vRes, double heigth, int quantity) {
        super();
        this.offset = offset;
        this.hRes = hRes;
        this.vRes = vRes;
        this.heigth = heigth;
        this.quantity = quantity;
    }

    @Override
    public void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(offset);
        appendText(" ");
        appendText(hRes);
        appendText(" ");
        appendText(vRes);
        appendText(" ");
        appendText(heigth);
        appendText(" ");
        appendText(quantity);
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("offset", offset, 0);
        Validator.isMoreThanOrEqualTo("hRes", hRes, 0);
        Validator.isMoreThanOrEqualTo("vRes", vRes, 0);
        Validator.isMoreThanOrEqualTo("heigth", heigth, 0);
        Validator.isMoreThanOrEqualTo("quantity", quantity, 1);
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public int gethRes() {
        return hRes;
    }

    public void sethRes(int hRes) {
        this.hRes = hRes;
    }

    public int getvRes() {
        return vRes;
    }

    public void setvRes(int vRes) {
        this.vRes = vRes;
    }

    public double getHeigth() {
        return heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "StartPrintCmd [offset=" + offset + ", hRes=" + hRes + ", vRes=" + vRes + ", heigth=" + heigth + ", quantity=" + quantity + "]";
    }

}
