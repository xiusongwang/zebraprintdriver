package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class Speed extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Speed() {
        this(0);
    }

    public Speed(int speed) {
        setParameter(speed);
    }

    public String getCommand() {
        return "SPEED";
    }

    @Override
    public void validate() {
        Validator.isBetween("speed", getParameter(), 0, 5);
    }

    public int getSpeed() {
        return getParameter();
    }

    public void setSpeed(int speed) {
        setParameter(speed);
    }

    @Override
    public String toString() {
        return "Speed [speed=" + getParameter() + "]";
    }

}
