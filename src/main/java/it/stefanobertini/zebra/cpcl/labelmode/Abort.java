package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

/**
 * 
 * Implements the ABORT command<br/>
 * <br/>
 * Description:<br>
 * The ABORT command terminates a current control session without printing. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * ABORT
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Abort extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "ABORT";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "Abort []";
    }
}
