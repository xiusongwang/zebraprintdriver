package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.Orientation;

import java.util.ArrayList;
import java.util.List;

public class Concat extends AbstractCommand implements LabelModeCommandInterface {

    private static class ConcatText {
        private Font font;
        private double offset;
        private String text;

        public ConcatText(Font font, double offset, String text) {
            super();
            this.font = font;
            this.offset = offset;
            this.text = text;
        }

        public Font getFont() {
            return font;
        }

        public double getOffset() {
            return offset;
        }

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return "ConcatText [font=" + font + ", offset=" + offset + ", text=" + text + "]";
        }

    }

    private Orientation orientation;
    private Position position;
    private List<ConcatText> textList = new ArrayList<Concat.ConcatText>();

    public Concat(Position position) {
        this(Orientation.horizontal, position);
    }

    public Concat(Orientation orientation, Position position) {
        super();
        this.orientation = orientation;
        this.position = position;
    }

    public Concat addText(Font font, double offset, String text) {
        textList.add(new ConcatText(font, offset, text));
        return this;
    }

    public String getCommand() {
        return Orientation.vertical.equals(orientation) ? "VCONCAT" : "CONCAT";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        endLine();

        for (ConcatText concatText : textList) {
            appendText(concatText.getFont().getCommandLine());
            appendText(" ");
            appendText(concatText.getOffset());
            appendText(" ");
            appendText(concatText.getText());
            endLine();
        }

        appendText("ENDCONCAT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isNotEmpty("textList", textList);
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<ConcatText> getTextList() {
        return textList;
    }

    public void setTextList(List<ConcatText> textList) {
        this.textList = textList;
    }

    @Override
    public String toString() {
        return "Concat [orientation=" + orientation + ", position=" + position + ", textList=" + textList + "]";
    }
}
