package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.TextRotation;

/**
 * 
 * Implements the TEXT command<br/>
 * <br/>
 * Description:<br>
 * The TEXT command is used to place text on a label. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * TEXT {font} {size} {x} {y} {data}
 * </pre>
 * 
 * {font} The name/number of the font<br>
 * {size} The size of the font <br/>
 * {x} Horizontal starting position<br/>
 * {y} Vertical starting position<br/>
 * {data} The text to be printed<br/>
 * <br/>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Text extends AbstractCommand implements LabelModeCommandInterface {

    private TextRotation textRotation = TextRotation.horizontal;
    private Font font = new Font("4", 0);
    private Position position = new Position(0, 0);
    private String text = "";

    public Text() {
        super();
    }

    public Text(Font font, Position position, String text) {
        this(TextRotation.horizontal, font, position, text);
    }

    public Text(TextRotation textRotation, Font font, Position position, String text) {
        super();
        this.textRotation = textRotation;
        this.font = font;
        this.position = position;
        this.text = text;
    }

    public String getCommand() {
        return textRotation.getCode();
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(textRotation.getCode());
        appendText(" ");
        appendText(font.getCommandLine());
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText(text);
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("textRotation", textRotation);
        Validator.isRequired("position", position);
        Validator.isRequired("font", font);
        Validator.isRequired("text", text);
    }

    public TextRotation getTextRotation() {
        return textRotation;
    }

    public void setTextRotation(TextRotation textRotation) {
        this.textRotation = textRotation;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Text [textRotation=" + textRotation + ", font=" + font + ", position=" + position + ", text=" + text + "]";
    }

}
