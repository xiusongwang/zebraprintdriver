package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.beans.Position;

public class InverseLine extends Line {

    public InverseLine(Position topLeft, Position bottomRight) {
        super(topLeft, bottomRight, 1);
    }

    public InverseLine(Position topLeft, Position bottomRight, double width) {
        super(topLeft, bottomRight, width);
    }

    public String getCommand() {
        return "INVERSE-LINE";
    }

    @Override
    public String toString() {
        return "InverseLine [getTopLeft()=" + getTopLeft() + ", getBottomRight()=" + getBottomRight() + ", getWidth()=" + getWidth() + "]";
    }

}
