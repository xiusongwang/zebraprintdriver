package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.OutOfPaperMode;

public class OutOfPaper extends AbstractCommand implements LabelModeCommandInterface {

    private OutOfPaperMode outOfPaperMode;
    private int retries;

    public OutOfPaper() {
        this(OutOfPaperMode.purge, 2);
    }

    public OutOfPaper(OutOfPaperMode outOfPaperMode, int retries) {
        super();
        this.outOfPaperMode = outOfPaperMode;
        this.retries = retries;
    }

    public String getCommand() {
        return "OUT-OF-PAPER";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(outOfPaperMode.getCode());
        appendText(" ");
        appendText(retries);
        endLine();

    }

    @Override
    public void validate() {
        Validator.isRequired("onFeedMode", outOfPaperMode);
        Validator.isMoreThanOrEqualTo("retries", retries, 0);
    }

    /**
     * @return the outOfPaperMode
     */
    public OutOfPaperMode getOutOfPaperMode() {
        return outOfPaperMode;
    }

    /**
     * @param outOfPaperMode
     *            the outOfPaperMode to set
     */
    public void setOutOfPaperMode(OutOfPaperMode outOfPaperMode) {
        this.outOfPaperMode = outOfPaperMode;
    }

    /**
     * @return the retries
     */
    public int getRetries() {
        return retries;
    }

    /**
     * @param retries
     *            the retries to set
     */
    public void setRetries(int retries) {
        this.retries = retries;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OutOfPaper [outOfPaperMode=" + outOfPaperMode + ", retries=" + retries + "]";
    }

}
