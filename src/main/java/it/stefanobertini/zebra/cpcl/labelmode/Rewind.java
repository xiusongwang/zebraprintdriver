package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.OnOffMode;

public class Rewind extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    private OnOffMode onOffMode;

    public Rewind() {
        this(OnOffMode.on);
    }

    public Rewind(OnOffMode onOffMode) {
        super();
        this.onOffMode = onOffMode;
    }

    public String getCommand() {
        return OnOffMode.on.equals(onOffMode) ? "REWIND-ON" : "REWIND-OFF";
    }

    @Override
    public void validate() {
        Validator.isRequired("onOffMode", onOffMode);
    }

    /**
     * @return the onOffMode
     */
    public OnOffMode getOnOffMode() {
        return onOffMode;
    }

    /**
     * @param onOffMode
     *            the onOffMode to set
     */
    public void setOnOffMode(OnOffMode onOffMode) {
        this.onOffMode = onOffMode;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Rewind [onOffMode=" + onOffMode + "]";
    }

}
