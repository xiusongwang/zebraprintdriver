package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

public class BarcodeTextOff extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public BarcodeTextOff() {
        super();
    }

    public String getCommand() {
        return "BARCODE-TEXT OFF";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "BarcodeTextOff []";
    }
}
