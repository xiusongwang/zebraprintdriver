package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

public class Pace extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "PACE";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "Pace []";
    }
}
