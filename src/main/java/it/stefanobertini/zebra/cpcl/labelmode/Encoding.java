package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.EncodingType;

/**
 * 
 * Implements the ENCODING command<br/>
 * <br/>
 * Description:<br>
 * The ENCODING command specifies the encoding of data sent to the printer. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * ENCODING {encodingType}
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Encoding extends AbstractStringParameterCommand implements LabelModeCommandInterface {

    public Encoding() {
    }

    public Encoding(String encodingType) {
        setParameter(encodingType);
    }

    public Encoding(EncodingType encodingType) {
        this.setParameter(encodingType.getCode());
    }

    public String getCommand() {
        return "ENCODING";
    }

    @Override
    public void validate() {
        Validator.isRequired("encodingType", getParameter());
    }

    public String getEncodingType() {
        return getParameter();
    }

    public void setEncodingType(String encodingType) {
        setParameter(encodingType);
    }

    public void setEncodingType(EncodingType encodingType) {
        setParameter(encodingType.getCode());
    }

    @Override
    public String toString() {
        return "Encoding [encodingType=" + getParameter() + "]";
    }

}
