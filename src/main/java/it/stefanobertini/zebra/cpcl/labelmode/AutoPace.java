package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

public class AutoPace extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "AUTO-PACE";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "AutoPace []";
    }
}
