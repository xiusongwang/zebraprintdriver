package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractDoubleParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class PageWidth extends AbstractDoubleParameterCommand implements LabelModeCommandInterface {

    public PageWidth() {
        this(0);
    }

    public PageWidth(double width) {
        setParameter(width);
    }

    public String getCommand() {
        return "PAGE-WIDTH";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("width", getParameter(), 0);
    }

    public double getWidth() {
        return getParameter();
    }

    public void setWidth(double count) {
        setParameter(count);
    }

    @Override
    public String toString() {
        return "PageWidth [width=" + getParameter() + "]";
    }

}
