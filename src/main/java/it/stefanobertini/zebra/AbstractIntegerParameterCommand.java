package it.stefanobertini.zebra;

public abstract class AbstractIntegerParameterCommand extends AbstractCommand {

    private int parameter;

    public void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(parameter);
        endLine();
    }

    public int getParameter() {
        return parameter;
    }

    public void setParameter(int parameter) {
        this.parameter = parameter;
    }

}
