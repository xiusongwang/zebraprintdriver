package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.DefineFile;
import it.stefanobertini.zebra.enums.DefineFileTerminator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DefineFileTest {

    private List<String> getData() {
	List<String> data = new ArrayList<String>();
	data.add("1");
	data.add("2");
	data.add("3");

	return data;
    }

    @Test
    public void test1() {

	DefineFile command = new DefineFile("TEST.TXT", getData(), DefineFileTerminator.end);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! DF TEST.TXT");
	for (String text : getData()) {
	    output.printLn(text);
	}
	output.printLn("END");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	DefineFile command = new DefineFile("TEST.TXT", getData(), DefineFileTerminator.print);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! DF TEST.TXT");
	for (String text : getData()) {
	    output.printLn(text);
	}
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test3() {

	DefineFile command = new DefineFile("TEST.TXT", getData());

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! DF TEST.TXT");
	for (String text : getData()) {
	    output.printLn(text);
	}
	output.printLn("END");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void test4() {

	DefineFile command = new DefineFile(null, getData(), DefineFileTerminator.print);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void test5() {

	DefineFile command = new DefineFile("TEST.TXT", null, DefineFileTerminator.print);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void test6() {

	DefineFile command = new DefineFile("TEST.TXT", getData(), null);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void test7() {

	DefineFile command = new DefineFile("TEST.TXT", null);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! DF TEST.TXT");
	output.printLn("END");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	DefineFile command = new DefineFile();
	command = new DefineFile("");
	command.setFileName("");
	command.setData(null);
	command.setFileTerminator(DefineFileTerminator.end);
	command.getFileName();
	command.getData();
	command.getFileTerminator();
	command.toString();
    }
}
