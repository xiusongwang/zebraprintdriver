package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.Type;

import org.junit.Test;

public class TypeTest {

    @Test
    public void test() {
	Type command = new Type("TEST.TXT");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("TYPE TEST.TXT");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Type command = new Type();

	command.getCommandByteArray();
    }

    @Test
    public void testFake() {
	Type command = new Type();
	command.setFileName("");
	command.getFileName();
	command.toString();
    }
}
