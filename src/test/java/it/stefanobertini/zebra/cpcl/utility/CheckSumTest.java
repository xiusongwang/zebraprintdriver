package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.CheckSum;

import org.junit.Test;

public class CheckSumTest {

    @Test
    public void test() {
	CheckSum command = new CheckSum();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("CHECKSUM");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	CheckSum command = new CheckSum();
	command.toString();
    }
}
