package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.Beep;

import org.junit.Test;

public class BeepTest {

    @Test
    public void testDefault() {
	Beep command = new Beep();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("BEEP 0");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Beep command = new Beep(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("BEEP 10");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Beep command = new Beep(-1);

	command.getCommandByteArray();
    }

    @Test
    public void testFake() {
	Beep command = new Beep();
	command.setLength(0);
	command.getLength();
	command.toString();
    }
}
