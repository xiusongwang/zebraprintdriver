package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.TimeOut;

import org.junit.Test;

public class TimeOutTest {

    @Test
    public void testDefault() {
	TimeOut command = new TimeOut();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("TIMEOUT 0");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	TimeOut command = new TimeOut(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("TIMEOUT 10");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	TimeOut command = new TimeOut(-1);

	command.getCommandByteArray();
    }

    @Test
    public void testFake() {
	TimeOut command = new TimeOut();
	command.setTimeout(0);
	command.getTimeout();
	command.toString();
    }
}
