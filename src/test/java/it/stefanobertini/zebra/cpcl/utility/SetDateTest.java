package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.utility.SetDate;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class SetDateTest {

    @Test
    public void test() {
	SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
	Date time = new Date();

	SetDate command = new SetDate(time);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("SET-DATE " + format.format(time));
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test
    public void testFake() {
	SetDate command = new SetDate();
	command.setDate(null);
	command.getDate();
	command.setDate(new Date());
	command.getDate();
	command.toString();
    }
}
