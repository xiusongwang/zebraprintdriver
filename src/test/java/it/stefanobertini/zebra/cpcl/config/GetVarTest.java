package it.stefanobertini.zebra.cpcl.config;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.config.GetVar;
import it.stefanobertini.zebra.enums.ConfigurationKey;

import org.junit.Test;

public class GetVarTest {

    @Test
    public void test() {
	GetVar command = new GetVar(ConfigurationKey.applicationDate);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 getvar \"appl.date\"");

	assertCommand(output, command);
    }

    @Test(expected = RuntimeException.class)
    public void testBadKey() {
	new GetVar(ConfigurationKey.deviceRestoreDefaults);
    }

    @Test(expected = RuntimeException.class)
    public void testNullKey() {
	new GetVar(null);
    }

    @Test
    public void testToSting() {
	new GetVar(ConfigurationKey.applicationDate).toString();
    }
}
