package it.stefanobertini.zebra.cpcl.config;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.config.Do;
import it.stefanobertini.zebra.enums.ConfigurationKey;

import org.junit.Test;

public class DoTest {

    @Test
    public void test() {
	Do command = new Do(ConfigurationKey.deviceRestoreDefaults, "wlan");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 do \"device.restore_defaults\" \"wlan\"");

	assertCommand(output, command);
    }

    @Test
    public void testNoArgumento() {
	Do command = new Do(ConfigurationKey.deviceRestoreDefaults);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 do \"device.restore_defaults\" \"\"");

	assertCommand(output, command);
    }

    @Test(expected = RuntimeException.class)
    public void testBadKey() {
	new Do(ConfigurationKey.applicationDate, "");
    }

    @Test(expected = RuntimeException.class)
    public void testNullKey() {
	new Do(null, "");
    }

    @Test
    public void testToSting() {
	new Do(ConfigurationKey.deviceRestoreDefaults).toString();
    }
}
