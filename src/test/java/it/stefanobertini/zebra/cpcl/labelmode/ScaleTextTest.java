package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.ScaleText;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class ScaleTextTest {

    @Test
    public void test() {
	ScaleText command = new ScaleText(Orientation.horizontal, "PLL_LAT.CSF", 10, 10, new Position(0, 10), "10 POINT FONT");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SCALE-TEXT PLL_LAT.CSF 10 10 0 10 10 POINT FONT");

	assertCommand(output, command);
    }

}
