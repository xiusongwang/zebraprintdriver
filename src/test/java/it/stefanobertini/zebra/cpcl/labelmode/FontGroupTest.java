package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.cpcl.labelmode.FontGroup;

import org.junit.Test;

public class FontGroupTest {

    @Test
    public void test() {
	FontGroup command = new FontGroup(1);
	command.addFont(new Font("4", 0));
	command.addFont("4", 1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("FG 1 4 0 4 1");

	assertCommand(output, command);
    }

}
