package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.PreFeed;

import org.junit.Test;

public class PreFeedTest {

    @Test
    public void testDefault() {
	PreFeed command = new PreFeed();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PREFEED 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	PreFeed command = new PreFeed(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PREFEED 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PreFeed command = new PreFeed(-1);

	command.getCommandByteArray();
    }

}
