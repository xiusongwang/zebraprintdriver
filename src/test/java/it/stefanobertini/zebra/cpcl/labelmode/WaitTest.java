package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Wait;

import org.junit.Test;

public class WaitTest {

    public void testDefault() {
	Wait command = new Wait();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("WAIT 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Wait command = new Wait(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("WAIT 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Wait command = new Wait(-1);

	command.getCommandByteArray();
    }

}
