package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.PostTension;

import org.junit.Test;

public class PostTensionTest {

    @Test
    public void testDefault() {
	PostTension command = new PostTension();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("POST-TENSION 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	PostTension command = new PostTension(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("POST-TENSION 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PostTension command = new PostTension(-1);

	command.getCommandByteArray();
    }

}
