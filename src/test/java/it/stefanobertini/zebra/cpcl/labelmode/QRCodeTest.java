package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.QRCodeAutomaticData;
import it.stefanobertini.zebra.beans.QRCodeDataInterface;
import it.stefanobertini.zebra.beans.QRCodeManualData;
import it.stefanobertini.zebra.beans.QRCodeManualDataItem;
import it.stefanobertini.zebra.cpcl.labelmode.QRCode;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.QRCodeCharacterMode;
import it.stefanobertini.zebra.enums.QRCodeErrorCorrectionLevel;
import it.stefanobertini.zebra.enums.QRCodeMaskNumber;
import it.stefanobertini.zebra.enums.QRCodeModelType;

import org.junit.Test;

public class QRCodeTest {

    @Test
    public void test1() {

	QRCodeDataInterface data;
	data = new QRCodeAutomaticData(QRCodeErrorCorrectionLevel.standard, QRCodeMaskNumber.none, "QR Code");

	QRCode command = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE QR 10 100 M 2 U 10");
	output.printLn("MA,QR Code");
	output.printLn("ENDQR");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	QRCodeManualData data;
	data = new QRCodeManualData(QRCodeErrorCorrectionLevel.ultraHighReliability, QRCodeMaskNumber.mask0);
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.numeric, "0123456789012345"));

	QRCode command = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE QR 10 100 M 2 U 10");
	output.printLn("H0M,N0123456789012345");
	output.printLn("ENDQR");

	assertCommand(output, command);
    }

    @Test
    public void test3() {

	QRCodeManualData data;
	data = new QRCodeManualData(QRCodeErrorCorrectionLevel.ultraHighReliability, QRCodeMaskNumber.mask0);
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.alphanumeric, "AC-42"));

	QRCode command = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE QR 10 100 M 2 U 10");
	output.printLn("H0M,AAC-42");
	output.printLn("ENDQR");

	assertCommand(output, command);
    }

    @Test
    public void test4() {

	QRCodeManualData data;
	data = new QRCodeManualData(QRCodeErrorCorrectionLevel.highDensity, QRCodeMaskNumber.none);
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.alphanumeric, "QR code"));
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.numeric, "0123456789012345"));
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.alphanumeric, "QRCODE"));
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.binary, "qrcode"));

	QRCode command = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE QR 10 100 M 2 U 10");
	output.printLn("LM,AQR code,N0123456789012345,AQRCODE,B0006qrcode");
	output.printLn("ENDQR");

	assertCommand(output, command);
    }

}