package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.ScalableConcat;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class ScalableConcatTest {

    @Test
    public void test() {
	ScalableConcat command = new ScalableConcat(Orientation.horizontal, new Position(0, 20));
	command.addBitmapText(new Font("4", 1), 0, "2/");
	command.addScalableText("PLL_LAT.CSF", 20, 20, 15, "$");
	command.addScalableText("PLL_LAT.CSF", 40, 40, 0, "22");
	command.addScalableText("PLL_LAT.CSF", 20, 20, 0, "99");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CONCAT 0 20");
	output.printLn("ST 4 1 0.0 2/");
	output.printLn("ST PLL_LAT.CSF 20 20 15.0 $");
	output.printLn("ST PLL_LAT.CSF 40 40 0.0 22");
	output.printLn("ST PLL_LAT.CSF 20 20 0.0 99");
	output.printLn("ENDCONCAT");

	assertCommand(output, command);
    }

}
