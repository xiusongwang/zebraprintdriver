package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.OnFeed;
import it.stefanobertini.zebra.enums.OnFeedMode;

import org.junit.Test;

public class OnFeedTest {

    @Test
    public void testDefault() {
	OnFeed command = new OnFeed();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ON-FEED FEED");

	assertCommand(output, command);
    }

    @Test
    public void testString() {
	OnFeed command = new OnFeed("FEED");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ON-FEED FEED");

	assertCommand(output, command);
    }

    @Test
    public void testEnum() {
	OnFeed command = new OnFeed(OnFeedMode.feed);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ON-FEED FEED");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	OnFeed command = new OnFeed();
	command.setOnFeedMode("");

	command.getCommandByteArray();
    }
}
