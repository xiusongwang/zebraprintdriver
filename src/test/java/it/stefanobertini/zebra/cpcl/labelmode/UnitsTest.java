package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Units;
import it.stefanobertini.zebra.enums.UnitsType;

import org.junit.Test;

public class UnitsTest {

    @Test
    public void testDefault() {
	Units command = new Units();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("IN-DOTS");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Units command = new Units(UnitsType.centimeters);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("IN-CENTIMETERS");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Units command = new Units(null);

	command.getCommandByteArray();
    }

}
