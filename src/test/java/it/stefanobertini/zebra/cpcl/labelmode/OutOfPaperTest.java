package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.OutOfPaper;
import it.stefanobertini.zebra.enums.OutOfPaperMode;

import org.junit.Test;

public class OutOfPaperTest {

    @Test
    public void testDefault() {
	OutOfPaper command = new OutOfPaper();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("OUT-OF-PAPER PURGE 2");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	OutOfPaper command = new OutOfPaper(OutOfPaperMode.wait, 5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("OUT-OF-PAPER WAIT 5");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	OutOfPaper command = new OutOfPaper(OutOfPaperMode.wait, -1);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void testValidation2() {
	OutOfPaper command = new OutOfPaper(null, 1);

	command.getCommandByteArray();
    }
}
