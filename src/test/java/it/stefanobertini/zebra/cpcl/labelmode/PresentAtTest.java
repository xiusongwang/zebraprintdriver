package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.PresentAt;

import org.junit.Test;

public class PresentAtTest {

    @Test
    public void testDefault() {
	PresentAt command = new PresentAt();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PRESENT-AT 0 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	PresentAt command = new PresentAt(1, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PRESENT-AT 1 2");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	PresentAt command = new PresentAt(-1, 0);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void testValidation2() {
	PresentAt command = new PresentAt(0, -1);

	command.getCommandByteArray();
    }

}
