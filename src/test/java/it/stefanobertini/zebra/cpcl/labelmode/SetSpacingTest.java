package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.SetSpacing;

import org.junit.Test;

public class SetSpacingTest {

    public void testDefault() {
	SetSpacing command = new SetSpacing();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SETSP 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	SetSpacing command = new SetSpacing(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SETSP 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	SetSpacing command = new SetSpacing(-1);

	command.getCommandByteArray();
    }

}
