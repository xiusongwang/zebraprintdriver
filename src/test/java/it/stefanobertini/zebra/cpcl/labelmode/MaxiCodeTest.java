package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.MaxiCode;
import it.stefanobertini.zebra.enums.MaxiCodeTag;

import org.junit.Test;

public class MaxiCodeTest {

    @Test
    public void test1() {
	MaxiCode command = new MaxiCode(new Position(1, 2));
	command.addTag(MaxiCodeTag.countryCode, "12345");
	command.addTag(MaxiCodeTag.msg, "This is a MAXICODE low priority message.");
	command.addTag(MaxiCodeTag.serviceClass, "12345");
	command.addTag(MaxiCodeTag.postalCode, "02886");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE MAXICODE 1 2");
	output.printLn("CC 12345");
	output.printLn("MSG This is a MAXICODE low priority message.");
	output.printLn("SC 12345");
	output.printLn("POST 02886");
	output.printLn("ENDMAXICODE");

	assertCommand(output, command);
    }

    @Test
    public void test2() {
	MaxiCode command = new MaxiCode(new Position(1, 2));
	command.addTag(MaxiCodeTag.countryCode, "12345");
	command.addTag(MaxiCodeTag.msg, "This is a MAXICODE low priority message.");
	command.addTag(MaxiCodeTag.serviceClass, "12345");
	command.addTag(MaxiCodeTag.postalCode, "02886");
	command.addTag(MaxiCodeTag.zipper, "1");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE MAXICODE 1 2");
	output.printLn("CC 12345");
	output.printLn("MSG This is a MAXICODE low priority message.");
	output.printLn("SC 12345");
	output.printLn("POST 02886");
	output.printLn("ZIPPER 1");
	output.printLn("ENDMAXICODE");

	assertCommand(output, command);
    }

}
