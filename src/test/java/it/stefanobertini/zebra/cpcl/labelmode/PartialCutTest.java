package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.PartialCut;

import org.junit.Test;

public class PartialCutTest {

    @Test
    public void test() {
	PartialCut command = new PartialCut();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PARTIAL-CUT");

	assertCommand(output, command);
    }

}
