package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.SetLinePrintTimeout;

import org.junit.Test;

public class SetLinePrintTimeoutTest {

    @Test
    public void test() {
	SetLinePrintTimeout command = new SetLinePrintTimeout(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETLP-TIMEOUT 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetLinePrintTimeout command = new SetLinePrintTimeout();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETLP-TIMEOUT 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	SetLinePrintTimeout command = new SetLinePrintTimeout(-1);

	command.getCommandByteArray();
    }
}
