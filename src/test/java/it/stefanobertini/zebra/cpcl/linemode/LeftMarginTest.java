package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.LeftMargin;

import org.junit.Test;

public class LeftMarginTest {

    @Test
    public void test() {
	LeftMargin command = new LeftMargin(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 LMARGIN 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	LeftMargin command = new LeftMargin();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 LMARGIN 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	LeftMargin command = new LeftMargin(-1);

	command.getCommandByteArray();
    }

}
