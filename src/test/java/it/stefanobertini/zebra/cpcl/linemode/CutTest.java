package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.linemode.Cut;

import org.junit.Test;

public class CutTest {

    @Test
    public void test() {
	Cut command = new Cut();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 CUT");

	assertCommand(output, command);
    }

}
